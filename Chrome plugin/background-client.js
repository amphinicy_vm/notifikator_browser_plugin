/**
 *  Copyright (c) 2017 Amphinicy Technologies.
 *  This script runs in the background as soon as Chrome is started
 */

const DEFAULT_WS = '192.168.210.140:3001';

setDefaultValues();

chrome.browserAction.setBadgeBackgroundColor({ color: '#FF0000' });

var ws = localStorage.ws,
	socket,
	warningId = 'latest';

connectMe();

// Listens for the change on the localStorage and updates the parameters accordingly
window.addEventListener('storage', function(e) {
	if(e.key === "username") {
		socket.emit('join', localStorage.username);
	}
	if(e.key === "ws") {
		ws = localStorage.ws;
		socket.disconnect();
		connectMe();
	}
	if(e.key === "requestRefresh" && localStorage.requestRefresh) {
		socket.emit('refreshMe');
		localStorage.requestRefresh = false;
	}

	setIcon();
});

function connectMe(){
	socket = io.connect('http://'+ws, {'forceNew': true});

	socket.on('connect', function(data){
		socket.emit('welcomemessagesetting', localStorage.welcomeMessage);
		socket.emit('join', localStorage.username);
		socketError(false);
	});

	socket.on('connect_error', function(error) {
		socketError(true);
	});

	socket.on('notification', function(data){
		var type = (data.image)?'image':'basic';

		var context = 'LP Admin';
		if (data.from === 'visnja') context = 'Teta Višnja';
		if (data.from === 'moneyman') context = 'MoneyMan';

		var iconUrl = 'logo.png';
		if (data.from === 'visnja') iconUrl = 'teta_visnja.png';
		if (data.from === 'moneyman') iconUrl = 'bat.jpg';
		if (data.from === 'admin') iconUrl = 'icons/icon.png';

		var options = {
			title: data.title,
			type: type,
			iconUrl: chrome.runtime.getURL('images/' + iconUrl),
			message: data.message,
			contextMessage: context,
			isClickable: true,
			requireInteraction: true
		};

		if ( data.image ) options.imageUrl = data.image.replace('https:', 'http:');

		// Reusing context as an 'id' (first param). This means that we can have max of 3 notifications shown at the same time and new notifications with same context will overwrite old notifications.
		chrome.notifications.create(context, options, function() {});
	});

	socket.on('activeLunchGroup', function(group){
		localStorage.activeLunchGroup = group;
		chrome.browserAction.setBadgeText({ text: group });
		if ( group === "" ) { delete localStorage.activeLunchGroup; }
	});

	socket.on('messages', function(data){
		if(data.settings) {
			Object.keys(data.settings).forEach(function(currentKey) {
				// Set "NEW" icon only if the update is for dailynews or weekmenu
				if( ("dailynews, weekmenu".indexOf(currentKey) >= 0) && (localStorage[currentKey] !== data.settings[currentKey]) ){
					localStorage.newInfo = true;
				}
				localStorage[currentKey] = data.settings[currentKey];
			});
		}
		setIcon();
	});
}

function socketError(isSocketDown) {
	localStorage.socketDown = isSocketDown;

	setIcon();
}

function setIcon(status) {
	if ( localStorage.socketDown === "true" ) {
		chrome.browserAction.setIcon({path: "images/icons/icon_error.png"});
	} else if ( localStorage.newInfo === "true" ){
		chrome.browserAction.setIcon({path: "images/icons/icon_new.png"});
	} else if ( localStorage.owesMoney === "true" ){
		chrome.browserAction.setIcon({path: "images/icons/icon_warning.png"});
	} else {
		chrome.browserAction.setIcon({path: "images/icons/icon.png"});
	}	
}

function setDefaultValues() {
	if ( !localStorage.dailynews ) localStorage.dailynews = "No news yet!";
	if ( !localStorage.lunchgroups ) localStorage.lunchgroups = "{}";
	if ( !localStorage.newInfo ) localStorage.newInfo = "false";
	if ( !localStorage.owesMoney ) localStorage.owesMoney = "false";
	if ( !localStorage.socketDown ) localStorage.socketDown = "false";
	if ( !localStorage.username ) localStorage.username = 'Anonimus';
	if ( !localStorage.weekmenu ) localStorage.weekmenu = "No menu yet!";
	if ( !localStorage.ws) localStorage.ws = DEFAULT_WS;
	if ( localStorage.welcomeMessage === undefined ) localStorage.welcomeMessage = true;
}