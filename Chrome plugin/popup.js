/**
 *	Copyright (c) 2017 Amphinicy Technologies.
 */

const 	DEFAULT_WS = '192.168.210.140:3001',
		DEFAULT_USERNAME = 'Anonimus';

var body, 
	buttons, 
	settingsBtns, 
	settings, 
	main, 
	usernameView, //on the main view
	usernameSettings, // in the settings
	ws, 
	weekMenu, 
	lunchSchedule,
	notificationTestButton,
	notificationTestImageButton,
	apmBtn;

window.onload = function() {
	body = document.getElementsByTagName("body")[0];
	buttons = document.getElementsByTagName('button');
	refreshBtns = document.getElementsByClassName('refresh-btn');
	main = document.getElementById('main');
	usernameView = document.getElementById('username');
	usernameSettings = document.getElementById('user-name');
	ws = document.getElementById('ws');
	welcomeMessage = document.getElementById('welcome-message');
	weekMenu = document.getElementById('week-menu-wrapper');
	lunchSchedule = document.getElementById('lunch-schedule-wrapper');
	settings = document.getElementById('settings-wrapper');
	notificationTestButton = document.getElementById('test-notification');
	notificationTestImageButton = document.getElementById('test-notification-image');
	apmBtn = document.getElementById('open-apm');


	setData();

	// Showing random Chuck notification
	notificationTestButton.addEventListener('click', function() {
		httpGetAsync("https://api.chucknorris.io/jokes/random", showTestNotification);
	}, false);

	// Showing random image notification
	notificationTestImageButton.addEventListener('click', function() {
		showTestNotification(JSON.stringify({
			image: "https://unsplash.it/300/200/?random&"+Date.now(),
			title: "Random picture",
			value: "unsplash.it"
		}));
	}, false);

	for(var i = 0; i < refreshBtns.length; i++) {
		refreshBtns[i].onclick = function() {
			console.log("refresh");
			this.classList.add("fa-spin");
			refreshData();
		}
	}

	// Navigating through the views
	for(var i = 0; i < buttons.length; i++) {
		var button = buttons[i];
		if(button.className.match(/\bleft\b/)) {
			button.onclick = function() {
				weekMenu.classList.add("not");
				lunchSchedule.classList.add("not");
				settings.classList.add("not");
				body.style.height = "auto";
				main.classList.remove("off");
			}
		}
		if(button.id === "week-menu-btn") {
			button.onclick = function() {
				weekMenu.classList.remove("not");
				var visina = weekMenu.offsetHeight;
				body.style.height = visina + "px";
				main.classList.add("off");
			}
		}
		if(button.id === "lunch-group-btn") {
			button.onclick = function() {
				lunchSchedule.classList.remove("not");
				var visina = lunchSchedule.offsetHeight;
				body.style.height = visina + "px";
				main.classList.add("off");
			}
		}
		if(button.id === "settings-btn") {
			button.onclick = function() {
				settings.classList.remove("not");
				var visina = settings.offsetHeight;
				body.style.height = visina + "px";
				main.classList.add("off");
			}
		}
	}

	apmBtn.onclick = function() {
		chrome.tabs.create({url: 'http://apm.amphinicy.com'});
	}

	// When changing name in the settings
	usernameSettings.addEventListener("blur", function(){
		if(usernameSettings.value.length != 0 && usernameSettings.value != localStorage.username) {
			localStorage.username = usernameSettings.value;
		} else if ( usernameSettings.value.length === 0 ) {
			localStorage.username = DEFAULT_USERNAME;
		}
		setMyName();
		drawLunchTable(JSON.parse(localStorage.lunchgroups));
	});

	// When changing ws address in the settings
	ws.addEventListener("blur", function(){
		if(ws.value.length != 0 && ws.value != localStorage.ws) {
			localStorage.ws = ws.value;
		} else if ( ws.value.length === 0 ) {
			localStorage.ws = DEFAULT_WS;
		}
	});

	welcomeMessage.addEventListener("change", function(){
		if (welcomeMessage.checked) { localStorage.welcomeMessage = true; }
		else { localStorage.welcomeMessage = false; }
	});

	// Event listener triggers only when the change is done in background process (background-client.js)
	window.addEventListener('storage', function(e) {
		console.log("Changed: " + e.key + " O: " + e. oldValue + " N: " + e.newValue);
		setData();
	});
}

function refreshData() {
	// This happens so fast user can't tell if the refresh happened or not! It's crazy! :D
	// So lets give it a small delay and let refresh icon to spin at least a bit.
	setTimeout(function(){ 
		localStorage.requestRefresh = true;
	}, 500);
}

function boolValue(data) {
	return (data == 'true');
}

function setData() {
	setMyName();

	if (!localStorage.ws) { localStorage.ws = DEFAULT_WS; } 
	else { ws.value = localStorage.ws; }

	if (!localStorage.username) { usernameSettings.username = DEFAULT_WS; } 
	else { usernameSettings.value = localStorage.username; }

	if (localStorage.welcomeMessage === undefined) { localStorage.welcomeMessage = true; } 
	else { welcomeMessage.checked = boolValue(localStorage.welcomeMessage); }

	if(localStorage.socketDown === "true") document.getElementById('socket-error').classList.remove("hide-me");
	else document.getElementById('socket-error').classList.add("hide-me");

	document.getElementById('daily-news').innerHTML = hideYoutubePlaceholder(localStorage.dailynews);
	document.getElementById('week-menu').innerHTML = hideYoutubePlaceholder(localStorage.weekmenu);

	var groupBadge = '';
	chrome.browserAction.getBadgeText({}, function(groupBadge) {
		if ( groupBadge ) {
			document.getElementById('active-group').innerHTML = groupBadge;
			document.getElementById('lunch-status').classList.remove("hide-me");
		} else {
			document.getElementById('lunch-status').classList.add("hide-me");
		}
	});

	if ( localStorage.owesMoney === 'true' ) document.getElementById('notice').classList.remove("hide-me");
	else document.getElementById('notice').classList.add("hide-me");

	drawLunchTable(JSON.parse(localStorage.lunchgroups));

	// When user opens the extension remove the new icon by setting the newInfo variable to false.
	localStorage.newInfo = false;

	stopRefreshSpinners();
}

function stopRefreshSpinners() {
	for(var i = 0; i < refreshBtns.length; i++) {
		refreshBtns[i].classList.remove("fa-spin");
	}
}

function drawLunchTable(lunchgroups) {
	var html = '';
	var today = new Date();
	today = today.yyyy_mm_dd() + ',' + today.getDayName();

	for (var day in lunchgroups) {
		if (!lunchgroups.hasOwnProperty(day)) continue;

		var table = '<table><tr class="table-head">';
		var dailySchedule = lunchgroups[day];
		var groupCount = Object.keys(dailySchedule).length;

		table += '<th colspan="'+groupCount+'">'+day+'</th></tr>';

		// Add row with group names and determine max number of members in a group
		var maxNamesInGroup = 0;
		table += '<tr>';
		for (var group in dailySchedule) {
			if(!dailySchedule.hasOwnProperty(group)) continue;

			var gUpCase = group.toUpperCase();
			gUpCase = gUpCase.replace("GROUP", "").trim();

			var nowClass = "";
			if ( localStorage.activeLunchGroup ) {
				var activeLunchGroup = localStorage.activeLunchGroup;
				if ( today === day && activeLunchGroup === gUpCase ) nowClass = "now";
			}

			if ( dailySchedule[group].length > maxNamesInGroup ) maxNamesInGroup = dailySchedule[group].length;
			table += '<th class="'+nowClass+'">'+group+'</th>';
		}
		table += '</tr>';

		// Create as many rows as the maxNamesInGroup is
		for (var i = 0; i < maxNamesInGroup; i++) {
			table += '<tr>';
			for (var group in dailySchedule) {
				if(!dailySchedule.hasOwnProperty(group)) continue;
				var isItMe = (dailySchedule[group][i] === localStorage.username)?'itsme':'';
				if ( dailySchedule[group][i] ) table += '<td class="'+isItMe+'">'+dailySchedule[group][i]+'</td>';
				else table += '<td>.</td>';
			}
			table += '</tr>';
		}

		table += '</table><br>';
		html += table;
	}

	document.getElementById('lunch-schedule').innerHTML = html;
}

// at the backend the YT video is shown in the editor as a picture placeholder so we have to swap that picture with the YT iframe
function hideYoutubePlaceholder (html) {
	const regex = /<img alt="youtube (\w+).+/g;
	var iframeTemplate = '<iframe class="video" style="width: 100%; height: auto; padding-bottom: 10px;" src="https://www.youtube.com/embed/VIDEOID?rel=0" frameborder="0" allowfullscreen></iframe>';

	// Remove all the unnecessary line breaks before img tag
	html = html.replace(/<br \/>\s<img/g, "<img");

	while ((m = regex.exec(html)) !== null) {
		var replaceStr, videoId;
		m.forEach(function(match, groupIndex) {
			if ( groupIndex === 0 ) replaceStr = match;
			else videoId = match;
		});

		var withStr = iframeTemplate.replace("VIDEOID", videoId);
		html = html.replace(replaceStr, withStr);

		// We moddified the indexes so lets start from the beginning
		regex.lastIndex = 0;
	}

	return html;
}

function setMyName() {
	if (!localStorage.username) { localStorage.username = DEFAULT_USERNAME; } // On localstorage change background process will invoke socket emit which will on return invoke this method again and the name will be set.
	else { usernameView.value = localStorage.username; }
	usernameView.textContent = localStorage.username;
}

function showTestNotification(data) {
	var data = JSON.parse(data);
	var type = (data.image)?'image':'basic';
	var options = {
		type: type,
		message: data.value,
		contextMessage: type + " notification",
		isClickable: true,
		requireInteraction: true
	};

	if ( data.icon_url ) options.iconUrl = data.icon_url.replace('https:', 'http:');
	else options.iconUrl = chrome.runtime.getURL('images/icons/icon.png');

	if ( data.image ) options.imageUrl = data.image.replace('https:', 'http:');

	if ( data.title ) options.title = data.title;
	else options.title = "Chuck fact!";

	chrome.notifications.create(type, options, function() {});
}

function httpGetAsync(theUrl, callback) {
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.onreadystatechange = function() { 
		if (xmlHttp.readyState == 4 && xmlHttp.status == 200) callback(xmlHttp.responseText);
	}
	xmlHttp.open("GET", theUrl, true);
	xmlHttp.send(null);
}

Date.prototype.yyyy_mm_dd = function() {
	var mm = this.getMonth() + 1;
	var dd = this.getDate();

	return [this.getFullYear(),
			(mm>9 ? '' : '0') + mm,
			(dd>9 ? '' : '0') + dd
		].join('-');
};

Date.prototype.getDayName = function() {
	var days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
	return days[this.getDay()];
}